from django.urls import path

from .api_views import api_list_presentations, api_show_presentation


urlpatterns = [
    path(
        "conferences/<int:conference_id>/presentations/",
        api_list_presentations,
        name="api_list_presentations",
    ),
    path(
        "presentations/<int:pk>/",
        api_show_presentation,
        name="api_show_presentation",
    ),
]

# The same can be said for presentations. We would generally
# only be interested in the presentations for a specific
# conference, not all presentations ever.
# Thus, the url would look like this:
# GET /api/conferences/<int:conference_id>/presentations/
# Gets a list of all presentations for a specific conference
