from django.urls import path

from .api_views import api_list_attendees, api_show_attendee

urlpatterns = [
    path(
        "conferences/<int:conference_id>/attendees/",
        api_list_attendees,
        name="api_list_attendees",
    ),
    path("attendees/<int:pk>/", api_show_attendee, name="api_show_attendee"),
]
# A dependent resource is one that we don't actually want to see all of
# in a list. It doesn't really make sense to get a list of all attendees,
# that is, a list of all the people signed up for every conference.
# What we generally want, in this example, is the list of attendees
# for a specific conference.# To do that, we specify which conference
# we want the attendees for in the url.Gets a list of all attendees
# for a specific conference If the url is /api/conferences/2/attendees/,
# then it would get the list of attendees for the conference that
# has an id of 2.# If the url is /api/conferences/1101/attendees/,
# then it would# get the list of attendees for the conference that
# has an id of 1101.
