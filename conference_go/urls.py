"""conference_go URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/", include("attendees.api_urls")),
    path("api/", include("events.api_urls")),
    path("api/", include("presentations.api_urls")),

]

# Method	 URL	                     What it does
# GET	     /api/conferences/	         Gets a list of all of the conferences
# GET	     /api/conferences/<int:pk>/	 Gets the details of one conference
# POST	/api/conferences/	     Create a new conference with the posted data
# PUT	     /api/conferences/<int:pk>/  Updates the details of one conference
# DELETE	 /api/conferences/<int:pk>/  Deletes a single conference


# Method	URL	                  What it does
# GET	    /api/«resource»s/
#                   Gets a list of all of the instances of «resource»
# GET	    /api/«resource»s/<int:pk>/
#                   Gets the details of one instance of «resource»
# POST	    /api/«resource»s/
#                   Create a new instance of «resource» with the posted data
# PUT	    /api/«resource»s/<int:pk>/
#                   Updates the details of one instance of «resource»
# DELETE	/api/«resource»s/<int:pk>/
#                   Deletes a single instance of «resource»
