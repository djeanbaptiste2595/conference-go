import requests
# import json
from events.keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

# ACL is known as anti coruption later, basically
# its a gate-keeper. We dont want to
# have additional information we dont need.


def get_lat_and_lon(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {"q": f"{city},{state},USA", "appid": OPEN_WEATHER_API_KEY}
    res = requests.get(url, params=params)
    geo_dict = res.json()
    lat = geo_dict[0]["lat"]
    lon = geo_dict[0]["lon"]
    return lat, lon


def get_weather_data(city, state):
    url = "https://api.openweathermap.org/data/2.5/weather"
    lat, lon = get_lat_and_lon(city, state)
    params = {
        "lat": lat, "lon": lon,
        "appid": OPEN_WEATHER_API_KEY
        }
    res = requests.get(url, params=params)
    weather_dict = res.json()
    temp = weather_dict['main']['temp']
    description = weather_dict['weather'][0]['description']

    return({"temp": temp, "description": description})


def get_photo(city, state):
    headers = {
        "Authorization": PEXELS_API_KEY
    }
    # For this API you need to set an authorization header with your request,
    # Need Authorization key
    url = "https://api.pexels.com/v1/search"
    # GET request to this particular URL
    params = {"query": f"{city} {state}", "per_page": 1}
    # Query is required parameter per API documentation
    # additional parameters are optional in this case
    res = requests.get(url, params=params, headers=headers)
    # returns a response from the request

    pexel_dict = res.json()
    # translates result from JSON to python list
    picture_url = pexel_dict["photos"][0]["src"]["original"]
    # go into list and nested dictionaries to find the picture URL you want
    # puts that picture url into picture_url variable
    return {"picture_url": picture_url}
